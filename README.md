## Simple Note REST API with Kotlin 

### Overview
Being developed in [develop](https://gitlab.com/sungorithm/simple-note-rest-api-kotlin/-/tree/develop) branch

> **Differences with [Simple Note REST API (Java ver.)](https://gitlab.com/sungorithm/simple-note-rest-api)**
> * No Lombok usage
> * Use UUID as ID instead of Long data type
> * No data wrapper object in request & response
> * Entity model constraint and validation rules
> * Return HTTP 404 status instead of 204 when entity not exists

### Prerequisites
* JDK 8
* Kotlin 1.5.32
* Maven

### Project Dependency & Plugins
* [Spring Boot Starter (Web / Data JPA / Validation / Test)](https://spring.io/projects/spring-boot)
* [H2 Database](https://www.h2database.com)
* [SpringMockK](https://github.com/Ninja-Squad/springmockk)

### Getting Started
1. create deployment jar file, artifact will be created in 'deployment' folder
```
mvn clean install
```

2. run executable jar file
```
java -jar simple-note-rest-api-kotlin.jar
```

&nbsp;
### API Component Design

| Class                                    | Layer             | Responsibility                                                           |
|------------------------------------------|:------------------|:-------------------------------------------------------------------------|
| SimpleNoteController                     | controller        | receive, validate request, and pass data for business process processing |
| SimpleNoteService, SimpleNoteServiceImpl | business service  | process business logics, throw app exception when an error occurred      |
| SimpleNoteRepository                     | repository        | handle data manipulation between service and database                    |
| SimpleNote                               | data model        | store request and response data (also ORM to database table)             |
| ErrorHandleController                    | controller advice | handle exception thrown from api processing                              |

&nbsp;
### API Usage

|  #  | Description               | Request Path       | HTTP Method | Request Body | HTTP Status | Response Body |
|:---:|:--------------------------|:-------------------|:-----------:|:------------:|:-----------:|:-------------:|
|  1  | Find all simple notes     | /simple-notes      |     GET     |      No      |     200     |      Yes      |
|  2  | Find exists simple note   | /simple-notes/{id} |     GET     |      No      |     200     |      Yes      |
|  3  | Create new simple note    | /simple-notes      |    POST     |     Yes      |     201     |      Yes      |
|  4  | Update exists simple note | /simple-notes/{id} |     PUT     |     Yes      |     200     |      Yes      |
|  5  | Delete exists simple note | /simple-notes/{id} |   DELETE    |      No      |     200     |      No       |

> **Remarks:**
> * HTTP status 403 returned without response body if request body is invalid
> * HTTP status 404 returned without response body if simple note is not found **(#2, #3, #5)**
> * HTTP status 500 returned without response body if unknown internal error occurred

&nbsp;
### Simple Note Data Model

|  #  | Field    | Description                          | Data type | Format / Length |
|:---:|:---------|:-------------------------------------|:---------:|:---------------:|
|  1  | id       | generated id of a simple note posted |   uuid    |      uuid       |
|  2  | story    | simple note story                    |  string   |   Text / 1-50   |
|  3  | detail   | note detail                          |  string   |  Text / 1-500   |
|  4  | dateTime | timestamp simple note posted         |  string   |  ISO Date Time  |

&nbsp;
### Sample Request & Response Body

Request body for creating **(#1)** and updating **(#2)** simple note, **no id required**
```
{
    "story": "Goodbye last year",
    "detail": "It was a good year",
    "dateTime": "2021-12-31T23:59:59"
}
```

\
Response body after created **(#1)** and updated **(#2)** simple note
```
{
    "id": "6b49c29b-4ee7-4d27-86a0-8929402ae0e0",
    "story": "Goodbye last year",
    "detail": "It was a good year",
    "dateTime": "2021-12-31T23:59:59"
}
```

\
Response body from finding all simple notes **(#3)**
```
[
    {
        "id": "6b49c29b-4ee7-4d27-86a0-8929402ae0e0",
        "story": "Goodbye last year",
        "detail": "It was a good year",
        "dateTime": "2021-12-31T23:59:59"
    },
    {
        "id": "5ae364e5-665a-4015-b1ad-f5b12fe5f867",
        "story": "Merry Christmas",
        "detail": "We wish you a merry christmas",
        "dateTime": "2021-12-24T21:00:01"
    }
]
```

\
Response body from finding exist simple note by id **(#4)**

```
{
    "id": "6b49c29b-4ee7-4d27-86a0-8929402ae0e0",
    "story": "Goodbye last year",
    "detail": "It was a good year",
    "dateTime": "2021-12-31T23:59:59"
}
```

&nbsp;
### Contact
Wasun Kaewpradub (SUN) - [wasun.kaewpradub@gmail.com](mailto:wasun.kaewpradub@gmail.com)

