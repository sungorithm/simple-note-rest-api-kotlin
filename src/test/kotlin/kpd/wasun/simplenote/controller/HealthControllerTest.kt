package kpd.wasun.simplenote.controller

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get

@WebMvcTest(controllers = [HealthController::class])
class HealthControllerTest(@Autowired val mockMvc: MockMvc) {

    @Test
    fun `Alive, then return http 200`() {
        mockMvc.get("/health").andExpect {
            status { isOk() }
            content { bytes(ByteArray(0)) }
        }
    }
}