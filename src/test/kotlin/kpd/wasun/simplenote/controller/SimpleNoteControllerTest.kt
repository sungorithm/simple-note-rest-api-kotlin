package kpd.wasun.simplenote.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import kpd.wasun.simplenote.entity.SimpleNote
import kpd.wasun.simplenote.exception.AppException
import kpd.wasun.simplenote.service.SimpleNoteService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

@WebMvcTest(controllers = [SimpleNoteController::class])
class SimpleNoteControllerTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objMapper: ObjectMapper
) {

    private lateinit var generatedId: UUID
    private lateinit var currentDateTime: LocalDateTime

    private fun createSimpleNote(id: UUID? = null) = SimpleNote(
        id = id,
        story = "Story name",
        detail = "Story detail",
        dateTime = currentDateTime
    )

    @MockkBean
    private lateinit var simpleNoteService: SimpleNoteService

    @BeforeEach
    fun setUp() {
        generatedId = UUID.randomUUID()
        currentDateTime = LocalDateTime.now()
    }

    @Test
    fun `Find by id, given exists id, then return http 200 with simple note`() {
        val simpleNote = createSimpleNote(generatedId)
        every { simpleNoteService.findById(generatedId) } returns simpleNote

        mockMvc.get("/simple-notes/$generatedId").andExpect {
            status { isOk() }
            content {
                contentType(MediaType.APPLICATION_JSON)
                json(objMapper.writeValueAsString(simpleNote))
            }
        }

        verify { simpleNoteService.findById(generatedId) }
    }

    @Test
    fun `Find by id, given not exists id, then return http 404`() {
        every { simpleNoteService.findById(generatedId) } throws AppException.ItemNotFound()

        mockMvc.get("/simple-notes/$generatedId").andExpect {
            status { isNotFound() }
            content { bytes(ByteArray(0)) }
        }

        verify { simpleNoteService.findById(generatedId) }
    }

    @Test
    fun `Find by id, given exists id but unknown error occurs, then return http 500`() {
        every { simpleNoteService.findById(generatedId) } throws Exception()

        mockMvc.get("/simple-notes/$generatedId").andExpect {
            status { isInternalServerError() }
            content { bytes(ByteArray(0)) }
        }

        verify { simpleNoteService.findById(generatedId) }
    }

    @Test
    fun `Find all, then return http 200 with list of simple notes`() {
        val simpleNotes = listOf(
            createSimpleNote(UUID.randomUUID()),
            createSimpleNote(UUID.randomUUID())
        )

        every { simpleNoteService.findAll() } returns simpleNotes

        mockMvc.get("/simple-notes").andExpect {
            status { isOk() }
            content {
                contentType(MediaType.APPLICATION_JSON)
                json(objMapper.writeValueAsString(simpleNotes))
            }
        }

        verify { simpleNoteService.findAll() }
    }

    @Test
    fun `Create, given simple note, then return http 201 with simple note and generated id`() {
        val newSimpleNote = createSimpleNote()
        val createdSimpleNote = createSimpleNote(generatedId)

        every { simpleNoteService.create(newSimpleNote) } returns createdSimpleNote

        mockMvc.post("/simple-notes") {
            contentType = MediaType.APPLICATION_JSON
            content = objMapper.writeValueAsString(newSimpleNote)
            accept = MediaType.APPLICATION_JSON

        }.andExpect {
            status { isCreated() }
            content {
                contentType(MediaType.APPLICATION_JSON)
                json(objMapper.writeValueAsString(createdSimpleNote))
            }
        }

        verify { simpleNoteService.create(newSimpleNote) }
    }

    @ParameterizedTest
    @CsvSource(
        nullValues = ["null"], value = [
            "null, story, 2022-01-01T00:00:00.000",
            "story, null, 2022-01-01T00:00:00.000",
            "story, detail, null"
        ]
    )
    fun `Create, given invalid simple note, then return http 403`(
        story: String?,
        detail: String?,
        dateTime: String?
    ) {
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")
        val newSimpleNote = SimpleNote(
            id = null,
            story = story,
            detail = detail,
            dateTime = dateTime?.let { LocalDateTime.parse(dateTime, formatter) }
        )

        mockMvc.post("/simple-notes") {
            contentType = MediaType.APPLICATION_JSON
            content = objMapper.writeValueAsString(newSimpleNote)
            accept = MediaType.APPLICATION_JSON

        }.andExpect {
            status { isBadRequest() }
            content { bytes(ByteArray(0)) }
        }
    }

    @Test
    fun `Update, given exists id and simple note, then return http 200 with updated simple note`() {
        val updatingSimpleNote = SimpleNote(
            id = null,
            story = "Updated story",
            detail = "Updated detail",
            dateTime = currentDateTime
        )

        val updatedSimpleNote = SimpleNote(
            id = generatedId,
            story = "Updated story",
            detail = "Updated detail",
            dateTime = currentDateTime
        )

        every { simpleNoteService.update(generatedId, updatingSimpleNote) } returns updatedSimpleNote

        mockMvc.put("/simple-notes/$generatedId") {
            contentType = MediaType.APPLICATION_JSON
            content = objMapper.writeValueAsString(updatingSimpleNote)
            accept = MediaType.APPLICATION_JSON

        }.andExpect {
            status { isOk() }
            content {
                contentType(MediaType.APPLICATION_JSON)
                json(objMapper.writeValueAsString(updatedSimpleNote))
            }
        }

        verify { simpleNoteService.update(generatedId, updatingSimpleNote) }
    }

    @Test
    fun `Delete, given exists id, then return http 200`() {
        every { simpleNoteService.deleteById(generatedId) } returns Unit

        mockMvc.delete("/simple-notes/$generatedId").andExpect {
            status { isOk() }
            content { bytes(ByteArray(0)) }
        }

        verify { simpleNoteService.deleteById(generatedId) }
    }
}