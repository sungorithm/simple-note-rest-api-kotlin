package kpd.wasun.simplenote

import kpd.wasun.simplenote.controller.HealthController
import kpd.wasun.simplenote.controller.SimpleNoteController
import kpd.wasun.simplenote.repository.SimpleNoteRepository
import kpd.wasun.simplenote.service.SimpleNoteService
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class SimpleNoteApplicationTests(
    @Autowired val simpleNoteRepository: SimpleNoteRepository,
    @Autowired val simpleNoteService: SimpleNoteService,
    @Autowired val simpleNoteController: SimpleNoteController,
    @Autowired val healthController: HealthController,
) {

    @Test
    fun contextLoads() {
        assertNotNull(simpleNoteRepository)
        assertNotNull(simpleNoteService)
        assertNotNull(simpleNoteController)
        assertNotNull(healthController)
    }
}
