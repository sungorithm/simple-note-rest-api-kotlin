package kpd.wasun.simplenote.repository

import kpd.wasun.simplenote.entity.SimpleNote
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.test.annotation.DirtiesContext
import java.time.LocalDateTime
import java.util.*

@DataJpaTest
@DirtiesContext(
    classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD
)
class SimpleNoteRepositoryTest(
    @Autowired val testEntityManager: TestEntityManager,
    @Autowired val simpleNoteRepository: SimpleNoteRepository
) {

    private lateinit var generatedId: UUID
    private lateinit var currentDateTime: LocalDateTime

    private fun createSimpleNote(id: UUID? = null) = SimpleNote(
        id = id,
        story = "Story name",
        detail = "Story detail",
        dateTime = currentDateTime
    )

    @BeforeEach
    fun setUp() {
        generatedId = UUID.randomUUID()
        currentDateTime = LocalDateTime.now()
    }

    @Test
    fun `Find by id, given exists id, then return simple note`() {
        val newSimpleNote = createSimpleNote()
        val generatedId = testEntityManager.persist(newSimpleNote).id

        val expectedSimpleNote = createSimpleNote(generatedId)
        val actualSimpleNoteOpt = generatedId?.let {
            simpleNoteRepository.findById(it)
        }

        assertEquals(expectedSimpleNote, actualSimpleNoteOpt?.get())
    }

    @Test
    fun `Find by id, given not exists id, then return optional of null`() {
        val actualSimpleNoteOpt = simpleNoteRepository.findById(generatedId)

        assertTrue(!actualSimpleNoteOpt.isPresent)
    }

    @Test
    fun `Find all, then return list of simple notes`() {
        val generatedId1 = testEntityManager.persist(createSimpleNote()).id
        val generatedId2 = testEntityManager.persist(createSimpleNote()).id

        val expectedSimpleNotes = listOf(
            createSimpleNote(generatedId1),
            createSimpleNote(generatedId2)
        )

        val actualSimpleNotes = simpleNoteRepository.findAll()
        assertEquals(expectedSimpleNotes, actualSimpleNotes)
    }

    @Test
    fun `Save, given simple note, then return simple note`() {
        val actualSimpleNote = simpleNoteRepository.save(createSimpleNote(null))
        val generatedId = actualSimpleNote.id
        val expectedSimpleNote = createSimpleNote(generatedId)

        assertEquals(expectedSimpleNote, actualSimpleNote)
    }

    @Test
    fun `Delete by id, given exists id, then not throw exception`() {
        val generatedId = testEntityManager.persist(createSimpleNote()).id
        assertDoesNotThrow { simpleNoteRepository.deleteById(generatedId!!) }
    }

    @Test
    fun `Delete by id, given not exists id, then throw empty result data access exception`() {
        assertThrows(EmptyResultDataAccessException::class.java) {
            simpleNoteRepository.deleteById(UUID.randomUUID())
        }
    }
}