package kpd.wasun.simplenote.service.impl

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.mockk.verifySequence
import kpd.wasun.simplenote.entity.SimpleNote
import kpd.wasun.simplenote.exception.AppException
import kpd.wasun.simplenote.repository.SimpleNoteRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import java.time.LocalDateTime
import java.util.*

@ExtendWith(MockitoExtension::class)
class SimpleNoteServiceImplTest {

    private lateinit var generatedId: UUID
    private lateinit var currentDateTime: LocalDateTime

    private val simpleNoteRepository: SimpleNoteRepository = mockk()
    private val simpleNoteService = SimpleNoteServiceImpl(simpleNoteRepository)

    private fun createSimpleNote(id: UUID? = null) = SimpleNote(
        id = id,
        story = "Story name",
        detail = "Story detail",
        dateTime = currentDateTime
    )

    @BeforeEach
    fun setUp() {
        generatedId = UUID.randomUUID()
        currentDateTime = LocalDateTime.now()
    }

    @Test
    fun `Find by id, given exists id, then return simple note`() {
        every { simpleNoteRepository.findById(generatedId) } returns Optional.of(createSimpleNote(generatedId))

        assertEquals(createSimpleNote(generatedId), simpleNoteService.findById(generatedId))
        verify { simpleNoteRepository.findById(generatedId) }
    }

    @Test
    fun `Find by id, given not exists id, then throw item not found exception`() {
        every { simpleNoteRepository.findById(generatedId) } returns Optional.empty()

        assertThrows(AppException.ItemNotFound::class.java) { simpleNoteService.findById(generatedId) }
        verify { simpleNoteRepository.findById(generatedId) }
    }

    @Test
    fun `Find all, then return list of simple notes`() {
        val generatedId1 = UUID.randomUUID()
        val generatedId2 = UUID.randomUUID()

        every {
            simpleNoteRepository.findAll()

        } returns listOf(
            createSimpleNote(generatedId1),
            createSimpleNote(generatedId2)
        )

        val expectedSimpleNotes = listOf(
            createSimpleNote(generatedId1),
            createSimpleNote(generatedId2)
        )
        val actualSimpleNotes = simpleNoteService.findAll()

        assertEquals(expectedSimpleNotes, actualSimpleNotes)
        verify { simpleNoteRepository.findAll() }
    }

    @Test
    fun `Create, given simple note, then return simple note with generated id`() {
        val inputSimpleNote = createSimpleNote()
        every { simpleNoteRepository.save(inputSimpleNote) } returns createSimpleNote(generatedId)

        assertEquals(createSimpleNote(generatedId), simpleNoteService.create(inputSimpleNote))
        verify { simpleNoteRepository.save(inputSimpleNote) }
    }

    @Test
    fun `Update, given exists id and simple note, then return updated simple note`() {
        val existsSimpleNote = createSimpleNote(generatedId)
        val updatingSimpleNote = createSimpleNote().apply {
            story = "Updated story"
            detail = "Updated detail"
            dateTime = currentDateTime
        }

        val updatedSimpleNote = createSimpleNote(generatedId).apply {
            story = "Updated story"
            detail = "Updated detail"
            dateTime = currentDateTime
        }

        every { simpleNoteRepository.findById(generatedId) } returns Optional.of(existsSimpleNote)
        every { simpleNoteRepository.save(updatedSimpleNote) } returns updatedSimpleNote

        assertEquals(updatedSimpleNote, simpleNoteService.update(generatedId, updatingSimpleNote))

        verifySequence {
            simpleNoteRepository.findById(generatedId)
            simpleNoteRepository.save(updatedSimpleNote)
        }
    }

    @Test
    fun `Update, given not exists id and simple note, then throw item not found exception`() {
        val updatingSimpleNote = createSimpleNote(generatedId).apply {
            story = "Updated story"
            detail = "Updated detail"
            dateTime = currentDateTime
        }

        every { simpleNoteRepository.findById(generatedId) } returns Optional.empty()

        assertThrows(AppException.ItemNotFound::class.java) {
            simpleNoteService.update(generatedId, updatingSimpleNote)
        }

        verify { simpleNoteRepository.findById(generatedId) }
    }

    @Test
    fun `Delete by id, given exists id, then not throw item not found exception`() {
        val existsSimpleNote = createSimpleNote(generatedId)

        every { simpleNoteRepository.findById(generatedId) } returns Optional.of(existsSimpleNote)
        every { simpleNoteRepository.deleteById(generatedId) } returns Unit

        assertDoesNotThrow { simpleNoteService.deleteById(generatedId) }

        verifySequence {
            simpleNoteRepository.findById(generatedId)
            simpleNoteRepository.deleteById(generatedId)
        }
    }

    @Test
    fun `Delete by id, given not exists id, then throw item not found exception`() {
        every { simpleNoteRepository.findById(generatedId) } returns Optional.empty()

        assertThrows(AppException.ItemNotFound::class.java) {
            simpleNoteService.deleteById(generatedId)
        }

        verify { simpleNoteRepository.findById(generatedId) }
    }
}