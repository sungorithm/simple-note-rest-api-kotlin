package kpd.wasun.simplenote.service.impl

import kpd.wasun.simplenote.entity.SimpleNote
import kpd.wasun.simplenote.exception.AppException
import kpd.wasun.simplenote.repository.SimpleNoteRepository
import kpd.wasun.simplenote.service.SimpleNoteService
import org.springframework.stereotype.Service
import java.util.*

@Service
class SimpleNoteServiceImpl(val simpleNoteRepository: SimpleNoteRepository) : SimpleNoteService {

    override fun findById(id: UUID): SimpleNote {
        return simpleNoteRepository.findById(id).orElseThrow { AppException.ItemNotFound() }
    }

    override fun findAll(): List<SimpleNote> {
        return simpleNoteRepository.findAll()
    }

    override fun create(simpleNote: SimpleNote): SimpleNote {
        return simpleNoteRepository.save(simpleNote)
    }

    override fun update(id: UUID, simpleNote: SimpleNote): SimpleNote {
        return if (simpleNoteRepository.findById(id).isPresent) {
            val updatingSimpleNote = simpleNote.apply { this.id = id }
            simpleNoteRepository.save(updatingSimpleNote)

        } else {
            throw AppException.ItemNotFound()
        }
    }

    override fun deleteById(id: UUID) {
        return if (simpleNoteRepository.findById(id).isPresent) {
            simpleNoteRepository.deleteById(id)
        } else {
            throw AppException.ItemNotFound()
        }
    }
}