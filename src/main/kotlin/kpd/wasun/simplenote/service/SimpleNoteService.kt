package kpd.wasun.simplenote.service

import kpd.wasun.simplenote.entity.SimpleNote
import java.util.*

interface SimpleNoteService {

    fun findById(id: UUID): SimpleNote

    fun findAll(): List<SimpleNote>

    fun create(simpleNote: SimpleNote): SimpleNote

    fun update(id: UUID, simpleNote: SimpleNote): SimpleNote

    fun deleteById(id: UUID)
}