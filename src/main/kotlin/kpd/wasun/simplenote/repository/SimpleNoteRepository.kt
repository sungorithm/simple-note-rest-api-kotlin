package kpd.wasun.simplenote.repository

import kpd.wasun.simplenote.entity.SimpleNote
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface SimpleNoteRepository : JpaRepository<SimpleNote, UUID>