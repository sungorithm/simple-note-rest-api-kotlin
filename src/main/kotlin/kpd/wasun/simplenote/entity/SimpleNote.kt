package kpd.wasun.simplenote.entity

import java.time.LocalDateTime
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Null
import javax.validation.constraints.Size

@Entity
@Table(name = "simple_note")
class SimpleNote(

    @field:Null
    @Id @GeneratedValue
    var id: UUID?,

    @field:NotBlank
    @field:Size(min = 1, max = 50)
    var story: String?,

    @field:NotBlank
    @field:Size(min = 1, max = 500)
    var detail: String?,

    @field:NotNull
    var dateTime: LocalDateTime?
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SimpleNote

        if (id != other.id) return false
        if (story != other.story) return false
        if (detail != other.detail) return false
        if (dateTime != other.dateTime) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (story?.hashCode() ?: 0)
        result = 31 * result + (detail?.hashCode() ?: 0)
        result = 31 * result + (dateTime?.hashCode() ?: 0)
        return result
    }
}