package kpd.wasun.simplenote.exception

sealed class AppException(override val message: String) : Exception(message) {

    class ItemNotFound : AppException("Item not found")
}