package kpd.wasun.simplenote.controller

import kpd.wasun.simplenote.exception.AppException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class ErrorHandleController {

    @ExceptionHandler(MethodArgumentNotValidException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleInvalidRequest() = null

    @ExceptionHandler(AppException::class)
    fun handleAppException(ae: AppException): ResponseEntity<Any> {
        val httpStatus = when (ae) {
            is AppException.ItemNotFound -> HttpStatus.NOT_FOUND
            /* Reserve for future usage */
        }

        return ResponseEntity.status(httpStatus).body(null)
    }

    @ExceptionHandler(Exception::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun handleUnknownException() = null
}