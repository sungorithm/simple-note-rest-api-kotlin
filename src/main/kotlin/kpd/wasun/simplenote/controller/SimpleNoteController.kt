package kpd.wasun.simplenote.controller

import kpd.wasun.simplenote.entity.SimpleNote
import kpd.wasun.simplenote.service.SimpleNoteService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/simple-notes")
class SimpleNoteController(val simpleNoteService: SimpleNoteService) {

    @GetMapping("/{id}")
    fun findById(@PathVariable id: UUID) = simpleNoteService.findById(id)

    @GetMapping
    fun findAll() = simpleNoteService.findAll()

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@Valid @RequestBody simpleNote: SimpleNote) = simpleNoteService.create(simpleNote)

    @PutMapping("/{id}")
    fun update(@PathVariable id: UUID, @Valid @RequestBody simpleNote: SimpleNote) =
        simpleNoteService.update(id, simpleNote)

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: UUID) = simpleNoteService.deleteById(id)
}