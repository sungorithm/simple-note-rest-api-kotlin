package kpd.wasun.simplenote

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SimpleNoteRestApiKotlinApplication

fun main(args: Array<String>) {
    runApplication<SimpleNoteRestApiKotlinApplication>(*args)
}
